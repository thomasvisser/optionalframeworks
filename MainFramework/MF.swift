
import OptionalFramework
import OptionalFramework2
import OptionalFramework3
import OptionalFramework4
import OptionalFramework5
import OptionalFramework6
import OptionalFramework7
import OptionalFramework8
import OptionalFramework9
import OptionalFramework10
import OptionalFramework11
import OptionalFramework12
import OptionalFramework13
import OptionalFramework14
import OptionalFramework15
import OptionalFramework16
import OptionalFramework17
import OptionalFramework18
import OptionalFramework19
import OptionalFramework20

public struct MF {

    public let name: String

    public init(name: String) {
        self.name = name
    }

    public func result() -> String {
        // Only one check because it's quite expensive and we try to measure dload stuff
        guard NSClassFromString("OptionalFramework.OFMarker") != nil else {
            return "--"
        }

        return """
            \(OptionalFramework.OF(person: false).person)
                        \(OptionalFramework2.OF(person: false).person)
                        \(OptionalFramework3.OF(person: false).person)
                        \(OptionalFramework4.OF(person: false).person)
                        \(OptionalFramework5.OF(person: false).person)
                        \(OptionalFramework6.OF(person: false).person)
                        \(OptionalFramework7.OF(person: false).person)
                        \(OptionalFramework8.OF(person: false).person)
                        \(OptionalFramework9.OF(person: false).person)
                        \(OptionalFramework10.OF(person: false).person)
            """

//        \(OptionalFramework12.OF(person: false).person)
//        \(OptionalFramework13.OF(person: false).person)
//        \(OptionalFramework14.OF(person: false).person)
//        \(OptionalFramework15.OF(person: false).person)
//        \(OptionalFramework16.OF(person: false).person)
//        \(OptionalFramework17.OF(person: false).person)
//        \(OptionalFramework18.OF(person: false).person)
//        \(OptionalFramework19.OF(person: false).person)
//        \(OptionalFramework20.OF(person: false).person)
    }


}
