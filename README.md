# Sandbox for framework count & linking experiments

With the project in this repo, the impact of weak vs strong linking and the impact of the amount of (dynamic) frameworks on pre-main dylib loading time can be investigated.

## Set-up
1. Copy the contents of the `cocoapods-repo` folder to a CocoaPods repository in ~/.cocoapods/repos`.
2. Run `pod install` in the `project` directory
3. Build & run the Xcode project inside the `project` directory 

## Change framework count
To change the amount of frameworks, update the dependencies in `MainFramework.podspec`.

## Use weak linking
This is the default set-up. All `OptionalFramework` frameworks are weak dependencies of `MainFramework`.

### Exclude unused weak frameworks from binary
This is the default set-up

### Include unused weak frameworks from binary
Comment out the marked section at the bottom of `Podfile`. That logic would prevent CocoaPods from copying the unused weak frameworks to the binary.

## Use strong linking
Comment out the `spec.weak_frameworks` value in `MainFrameworks.podspec`.