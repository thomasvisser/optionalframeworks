#
#  Be sure to run `pod spec lint MainFramework.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "MainFramework"
  spec.version      = "0.0.1"
  spec.summary      = "A short description of MainFramework."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = <<-DESC
  test
                   DESC

  spec.homepage     = "http://EXAMPLE/MainFramework"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = { "Thomas Visser" => "thomas.visser@gmail.com" }
  # Or just: spec.author    = "Thomas Visser"
  # spec.authors            = { "Thomas Visser" => "thomas.visser@gmail.com" }
  # spec.social_media_url   = "https://twitter.com/Thomas Visser"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  spec.platform     = :ios, "13.0"
  # spec.platform     = :ios, "5.0"

  #  When using multiple platforms
  # spec.ios.deployment_target = "5.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  spec.source       = { :git => "http://MainFramework.git", :tag => "#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  spec.source_files  = "MainFramework/**/*.{h,m,swift}"

  # spec.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  spec.dependency "OptionalFramework"
  spec.dependency "OptionalFramework2"
  spec.dependency "OptionalFramework3"
  spec.dependency "OptionalFramework4"
  spec.dependency "OptionalFramework5"
  spec.dependency "OptionalFramework6"
  spec.dependency "OptionalFramework7"
  spec.dependency "OptionalFramework8"
  spec.dependency "OptionalFramework9"
  spec.dependency "OptionalFramework10"
  spec.dependency "OptionalFramework11"
  spec.dependency "OptionalFramework12"
  spec.dependency "OptionalFramework13"
  spec.dependency "OptionalFramework14"
  spec.dependency "OptionalFramework15"
  spec.dependency "OptionalFramework16"
  spec.dependency "OptionalFramework17"
  spec.dependency "OptionalFramework18"
  spec.dependency "OptionalFramework19"
  spec.dependency "OptionalFramework20"
  spec.dependency "OptionalFramework21"
  spec.dependency "OptionalFramework22"
  spec.dependency "OptionalFramework23"
  spec.dependency "OptionalFramework24"
  spec.dependency "OptionalFramework25"
  spec.dependency "OptionalFramework26"
  spec.dependency "OptionalFramework27"
  spec.dependency "OptionalFramework28"
  spec.dependency "OptionalFramework29"
  spec.dependency "OptionalFramework30"
  spec.dependency "OptionalFramework31"
  spec.dependency "OptionalFramework32"
  spec.dependency "OptionalFramework33"
  spec.dependency "OptionalFramework34"
  spec.dependency "OptionalFramework35"
  spec.dependency "OptionalFramework36"
  spec.dependency "OptionalFramework37"
  spec.dependency "OptionalFramework38"
  spec.dependency "OptionalFramework39"
  spec.dependency "OptionalFramework40"

  spec.weak_frameworks = [
    "OptionalFramework", "OptionalFramework2", "OptionalFramework3", "OptionalFramework4", "OptionalFramework5", "OptionalFramework6", "OptionalFramework7", "OptionalFramework8", "OptionalFramework9", "OptionalFramework10",
    "OptionalFramework11", "OptionalFramework12", "OptionalFramework13", "OptionalFramework14", "OptionalFramework15", "OptionalFramework16", "OptionalFramework17", "OptionalFramework18", "OptionalFramework19", "OptionalFramework20",
    "OptionalFramework21", "OptionalFramework22", "OptionalFramework23", "OptionalFramework24", "OptionalFramework25", "OptionalFramework26", "OptionalFramework27", "OptionalFramework28", "OptionalFramework29", "OptionalFramework30",
    "OptionalFramework31", "OptionalFramework32", "OptionalFramework33", "OptionalFramework34", "OptionalFramework35", "OptionalFramework36", "OptionalFramework37", "OptionalFramework38", "OptionalFramework39", "OptionalFramework40"
  ]

end
