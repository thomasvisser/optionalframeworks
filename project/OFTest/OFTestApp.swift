//
//  OFTestApp.swift
//  OFTest
//
//  Created by Thomas Visser on 21/07/2021.
//

import SwiftUI

@main
struct OFTestApp: App {
    init() {
        print("test")
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
