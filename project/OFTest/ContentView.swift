//
//  ContentView.swift
//  OFTest
//
//  Created by Thomas Visser on 21/07/2021.
//

import SwiftUI
import MainFramework

struct ContentView: View {
    @State var text = "?"

    var body: some View {
        VStack {
            Text("Hello, world!")
                .padding()

            Text(text)
        }
        .onAppear {
            let d = Date()
            let t = MF(name: "Test").result()
            print("Result took \(d.timeIntervalSinceNow * -1)")

            self.text = t
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
